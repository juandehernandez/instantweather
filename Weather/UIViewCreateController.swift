//
//  UIViewCreateController.swift
//  Notify
//
//  Created by Alumne on 29/10/2019.
//  Copyright © 2019 Alumne. All rights reserved.
//

import UIKit
import MapKit

class UIViewCreateController: UIViewController {

    @IBOutlet weak var recorlabel: UILabel!
    @IBOutlet weak var recorText: UITextField!
    @IBOutlet weak var recorCancel: UIButton!
    @IBOutlet weak var recorDone: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var newLocationCreated: (_ newLocation: CLPlacemark) -> Void  = { arg in};
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recorDone.layer.cornerRadius = 10;
        // Do any additional setup after loading the view.
    }
    

    private func makeErrorAlert(){
        let alert = UIAlertController(title: "Error", message: "To search for a location you need to put a location", preferredStyle: .alert);
    
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil));
    
        self.present(alert,animated: true);
    }
    
    private func geolocateUserInput(_ string: String) {
        let geocoder = CLGeocoder();
        geocoder.geocodeAddressString(string, completionHandler: {(placemark,error) in
            self.spinner.stopAnimating();
            self.recorDone.isHidden = false;
            if(error == nil){
                self.newLocationCreated((placemark?.first)!);
                
                /*APIManager.shared.requestWeatherForCity((placemark?.first?.name)!, (placemark?.first?.isoCountryCode)!, callback: { (ResponseData) in
                    print(ResponseData.description)
                })*/
                
                /*APIManager.shared.requestWeatherForecastForCity((placemark?.first?.name)!, (placemark?.first?.isoCountryCode)!, callback: { (ResponseData) in
                    print(ResponseData.description)
                })*/
                
                self.dismiss(animated: true, completion: nil);
            }else{
                let alert = UIAlertController(title: "Error en ubicación", message: "La ubicación no se ha encontrado", preferredStyle: .actionSheet);
                alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        })
    }
    
    @IBAction func searchLocation(_ sender: Any) {
        
        if let recor = recorText.text {
            if(recor.count > 0){
                spinner.startAnimating();
                recorDone.isHidden = true;
                
                geolocateUserInput(recor);
            }else{
                self.makeErrorAlert();
            }
        }else{
            self.makeErrorAlert();
        }
    }
    
    
    @IBAction func cancelLocation(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
