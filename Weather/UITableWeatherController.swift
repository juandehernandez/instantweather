//
//  UITableTaskContoller.swift
//  Notify
//
//  Created by Alumne on 29/10/2019.
//  Copyright © 2019 Alumne. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import SwiftyTimer
import Lottie

class UITableWeatherContoller: UITableViewController {
    var actualGeolocation:ResponseData?
    var myLocations:[NSManagedObject] = [NSManagedObject]()
    
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        if locationManager == nil {
            locationManager = CLLocationManager()
        }
        locationManager!.delegate = self
        locationManager!.requestAlwaysAuthorization()
        locationManager!.startUpdatingLocation()
        locationManager!.requestLocation()
        print("viewdidload")
        //deleteAllData("Weather")
        Timer.every(10.minutes) {
            for location in self.myLocations {
                APIManager.shared.requestWeatherForCity(location.value(forKey: "name") as! String, location.value(forKey: "country") as! String, callback: { (ResponseData) in
                    print(ResponseData.description)
                    //self.saveLocation(location: ResponseData)
                    print("temp madafaka is: \(ResponseData.temp)")
                    DispatchQueue.main.async {
                        location.setValue(ResponseData.temp, forKey: "temp")
                        location.setValue(ResponseData.timezone, forKey: "timezone")
                        self.tableView.reloadData()
                        print("updated location")
                    }
                })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("111")
        // 1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        print("222")
        // 2
        let fetchRequest : NSFetchRequest<Weather> = Weather.fetchRequest()
        
        print("333")
        // 3
        do {
            let results = try managedContext.fetch(fetchRequest)
            myLocations = results as [NSManagedObject]
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
        }
        print("444")
        // 4
        tableView.reloadData()
        print("finished reload data")
    }
    
    private func getHour(timezone: Int) -> String {
        let currentDate = Date()
        
        let format = DateFormatter()
        format.dateFormat = "HH:mm"
        
        print("current date is \(currentDate)")
        print("time zone is \(timezone)")
        format.timeZone = TimeZone(secondsFromGMT: timezone)
        
        let dateString = "\(format.string(from: currentDate)) (\(timezone/3600))"
        
        
        return dateString
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1;
        } else {
            return myLocations.count;
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("check section for a cell")
        if indexPath.section == 0 {
            print("lll1")
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationItem", for: indexPath) as! UIWeatherTableViewCell;
            print("lll2")
            if let location = actualGeolocation?.name {
                cell.locationLabel.text = location
            }
            print("lll3")
            if let degree = actualGeolocation?.temp {
                cell.degreeLabel.text = String(degree) + "º"
            }
            
            print("lll4")
            if let timezone = actualGeolocation?.timezone {
                
                cell.hourLabel.text = getHour(timezone: timezone)
            }
            
            print("return cell for section 0")
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationItem", for: indexPath) as! UIWeatherTableViewCell;
            cell.locationLabel.text = myLocations[indexPath.row].value(forKey: "name") as? String
            print("temp is:::")
            print(myLocations[indexPath.row].value(forKey: "temp") as! Float)
            cell.degreeLabel.text = String(myLocations[indexPath.row].value(forKey: "temp") as! Float) + "º"
            
            //format.timeZone = TimeZone(secondsFromGMT: 3600 * myLocations[indexPath.row].value(forKey: "timezone"))
            
            //let dateString = format.string(from: currentDate)
            
            //cell.hourLabel.text = dateString
            
            print("lolol let init")
            print(myLocations[indexPath.row].value(forKey: "timezone")!)
            if let timezone = myLocations[indexPath.row].value(forKey: "timezone") as? Int {
                print("lolol let dins")
                print(timezone)
                
                cell.hourLabel.text = getHour(timezone: timezone)
            }
            
            return cell
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("DELETE")
            deleteLocation(row: indexPath.row)
            tableView.reloadData()
        }
    }


    /*

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
  


    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }



    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }



    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }


     */
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "createLocation",
            let creator = segue.destination as? UIViewCreateController {
            
            creator.newLocationCreated = { newLocation in
                APIManager.shared.requestWeatherForCity((newLocation.name)!, (newLocation.isoCountryCode)!, callback: { (ResponseData) in
                    print(ResponseData.description)
                    DispatchQueue.main.async {
                        
                        self.saveLocation(location: ResponseData)
                        self.tableView.reloadData()
                        print("saved location")
                    }
                })
                //self.myLocations.append(newLocation);
                //self.tableView.reloadData();
            }
        } else if segue.identifier == "showLocation",
            let creator = segue.destination as? UILocationTableViewController,
            let indexPath = tableView.indexPathForSelectedRow {
            let row = indexPath.row
            
            if indexPath.section == 0 {
                creator.city = actualGeolocation!.name
                creator.main = actualGeolocation!.main
                creator.country = actualGeolocation!.country
                creator.temp = actualGeolocation!.temp
                
            } else {
                creator.city = myLocations[row].value(forKey: "name") as! String
                creator.main = myLocations[row].value(forKey: "main") as! String
                creator.country = myLocations[row].value(forKey: "country") as! String
                creator.temp = myLocations[row].value(forKey: "temp") as! Float
            }
        }
    }
    
    func saveLocation(location: ResponseData) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let weatherEntity = NSEntityDescription.entity(forEntityName: "Weather", in: managedContext)!
        
        let weather = NSManagedObject(entity: weatherEntity, insertInto: managedContext)
        print(location.name)
        weather.setValue(location.name, forKeyPath: "name")
        weather.setValue(location.temp, forKeyPath: "temp")
        weather.setValue(location.main, forKeyPath: "main")
        weather.setValue(location.country, forKeyPath: "country")
        weather.setValue(location.timezone, forKeyPath: "timezone")
        
        do {
            
            myLocations.append(weather)
            try managedContext.save()
            
            print(weather.value(forKey: "name")!)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func deleteLocation(row: Int) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        //fetchRequest.predicate = NSPredicate(format: "name = %@", cityName)
        
        managedContext.delete(myLocations[row])
        
        do {
            try managedContext.save()
            myLocations.remove(at: row)
        } catch {
            print(error)
        }
        
        /*do {
            //let test = try managedContext.fetch(fetchRequest)
            
            //let objectToDelete = test[0] as! NSManagedObject
            
            managedContext.delete(myLocations[row])
            myLocations.remove(at: row)
            
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }*/
    }
    
    func deleteAllData(_ entity: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Delete all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    /*func retrieveData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "name") as! String)
            }
        } catch {
            print("Failed")
        }
    }*/


}


extension UITableWeatherContoller: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (locations.count == 0) {return;}
        
        let location = locations.first!;
        
        let geocoder = CLGeocoder();
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemark,error) in
            APIManager.shared.requestWeatherForCity((placemark?.first?.subAdministrativeArea)!, (placemark?.first?.isoCountryCode)!, callback: { (ResponseData) in
                print(ResponseData.description)
                DispatchQueue.main.async {
                    //self.myLocations.insert(ResponseData, at: 0)
                    print("lets save location")
                    self.actualGeolocation = ResponseData
                    self.tableView.reloadData()
                }
            })
        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}

extension UITableWeatherContoller: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension String {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
