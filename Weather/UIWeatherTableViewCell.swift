//
//  UITaskTableViewCell.swift
//  Notify
//
//  Created by Alumne on 29/10/2019.
//  Copyright © 2019 Alumne. All rights reserved.
//

import UIKit

class UIWeatherTableViewCell: UITableViewCell {
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
