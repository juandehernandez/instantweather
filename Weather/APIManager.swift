//
//  APIManager.swift
//  NetworkDemo
//
//  Created by Alex Tarragó on 18/11/2019.
//  Copyright © 2019 Dribba GmbH. All rights reserved.
//

import Foundation


//https://openweathermap.org/current
private let baseURL = "https://api.openweathermap.org/data/2.5/weather?appid=3800aab413275ef765b5262a5072ce84&units=metric&q="
private let forecastURL = "https://api.openweathermap.org/data/2.5/forecast?appid=3800aab413275ef765b5262a5072ce84&units=metric&q="


class APIManager {
    static let shared = APIManager()
    
    init(){ }

    // Network requests
    func requestWeatherForCity(_ city: String, _ countryCode: String, callback: @escaping (_ data: ResponseData) -> Void) {
        
        let citys = city.replacingOccurrences(of: " ", with: "%20")
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(baseURL)\(citys),\(countryCode)")! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        
        request.httpMethod = "GET"

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                
                if let data = data {
                  if let jsonString = String(data: data, encoding: .utf8) {
                    
                    if(jsonString.contains("city not found")){
                        return
                    }
                    
                    let data = self.convertToDictionary(text: jsonString)
                    
                    let responseData = self.exportResource(data: data)
                    
                    callback(responseData)
                    
                  }
                }
            }
            })
        dataTask.resume()
    }

    func requestWeatherForecastForCity(_ city: String, _ countryCode: String, callback: @escaping (_ data: [ResponseDataForecast]) -> Void){
        
        let citys = city.replacingOccurrences(of: " ", with: "%20")
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(forecastURL)\(citys),\(countryCode)")! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        
        request.httpMethod = "GET"

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                
                if let data = data {
                  if let jsonString = String(data: data, encoding: .utf8) {
                    
                    let data = self.convertToDictionary(text: jsonString)
                    
                    let arr = (data!["list"]! as! Array<Dictionary<String, Any>>)
                    var content: [ResponseDataForecast] = [];
                    for dat in arr {
                        content.append(self.exportResourceForecast(data: dat))
                    }
	            
                    callback(content)
                    
                  }
                }
            }
        })
        dataTask.resume()
    }
    
    // Helpers
    func exportResource(data: [String: Any]?) -> ResponseData {
        let name = data!["name"]
        let main = (data!["weather"]! as! Array<Dictionary<String, Any>>).first!["main"]!
        let desc = (data!["weather"]! as! Array<Dictionary<String, Any>>).first!["description"]!
        let temp = (data!["main"]! as! Dictionary<String, Any>)["temp"]!
        //let index = temp.index(temp.startIndex, offsetBy: 3)
        //let temp1decimal = temp[..<index]
        let miTp = "\((data!["main"]! as! Dictionary<String, Any>)["temp_min"]!)"
        let mxTp = "\((data!["main"]! as! Dictionary<String, Any>)["temp_max"]!)"
        let timezone = data!["timezone"]
        let country = "\((data!["sys"]! as! Dictionary<String, Any>)["country"]!)"
        return ResponseData(name: name as! String, main: main as! String, description: desc as! String, temp: (temp as! NSNumber).floatValue, tempMax: mxTp, tempMin: miTp, timezone: timezone as! Int, country: country)
    }
    
    func exportResourceForecast(data: [String: Any]?) -> ResponseDataForecast {
        let main = (data!["weather"]! as! Array<Dictionary<String, Any>>).first!["main"]!
        let temp = (data!["main"]! as! Dictionary<String, Any>)["temp"]!
        //let index = temp.index(temp.startIndex, offsetBy: 3)
        //let temp1decimal = temp[..<index]
        let miTp = (data!["main"]! as! Dictionary<String, Any>)["temp_min"]!
        let mxTp = (data!["main"]! as! Dictionary<String, Any>)["temp_max"]!
        let date = data!["dt_txt"]

        return ResponseDataForecast(main: main as! String, temp: (temp as! NSNumber).floatValue, tempMax: (mxTp as! NSNumber).floatValue, tempMin: (miTp as! NSNumber).floatValue, date: date as! String)
    }

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

struct ResponseData {
    var name: String
    var main: String
    var description: String
    var temp: Float
    var tempMax: String
    var tempMin: String
    var timezone: Int
    var country: String
}

struct ResponseDataForecast {
    var main: String
    var temp: Float
    var tempMax: Float
    var tempMin: Float
    var date: String
}
