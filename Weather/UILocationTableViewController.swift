//
//  UILocationViewController.swift
//  Weather
//
//  Created by Juan de la cruz Hernandez Garcia on 20/11/2019.
//  Copyright © 2019 Juan de la cruz Hernandez y Bernardo Nouel. All rights reserved.
//

import UIKit

class UILocationTableViewController: UITableViewController {
    var city:String = ""
    var temp:Float = 0.0
    var main:String = ""
    var country:String = ""
    var forecasts:[ResponseDataForecast] = [ResponseDataForecast]()
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityLabel.adjustsFontSizeToFitWidth = true
        cityLabel.minimumScaleFactor = 0.2
 
        cityLabel.text = city
        tempLabel.text = "\(temp)º"
        mainLabel.text = main
        
        APIManager.shared.requestWeatherForecastForCity(city, country, callback: { (ResponseDataForecasts) in
            print(ResponseDataForecasts[0].tempMin)
            for ResponseDataForecast in ResponseDataForecasts {
                DispatchQueue.main.async {
                    //We only add the forecast of midday that is hour 15:00
                    let firstIndex: String.Index = ResponseDataForecast.date.startIndex
                    if ResponseDataForecast.date[ResponseDataForecast.date.index(firstIndex, offsetBy: 11)] == "1" && ResponseDataForecast.date[ResponseDataForecast.date.index(firstIndex, offsetBy: 12)] == "5" {
                        self.forecasts.append(ResponseDataForecast)
                    }
                    print("saved location")
                    print(ResponseDataForecast.date)
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    private func getDayOfWeek(_ today:String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        
        if weekDay == 1 {
            return "Sunday"
        } else if weekDay == 2 {
            return "Monday"
        } else if weekDay == 3 {
            return "Tuesday"
        } else if weekDay == 4 {
            return "Wednesday"
        } else if weekDay == 5 {
            return "Thursday"
        } else if weekDay == 6 {
            return "Friday"
        } else if weekDay == 7 {
            return "Saturday"
        }
        
        return nil
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecasts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayForecast", for: indexPath) as! UILocationTableViewCell;
        cell.tempMax.text = "\(forecasts[indexPath.row].tempMax)º"
        cell.tempMin.text = "\(forecasts[indexPath.row].tempMin)º"
        
        print("day is:")
        print(forecasts[indexPath.row].date)
        print("fiday")
        print(getDayOfWeek(forecasts[indexPath.row].date)!)
        print("inici let")
        if let day = getDayOfWeek(forecasts[indexPath.row].date) {
            print("dins let")
            print(day)
            cell.dayLabel.text = day
        }
        print("after day")
        
        if forecasts[indexPath.row].main == "Clouds" {
            cell.weatherImage?.image = UIImage(named: "ic_cloud")
            cell.weatherImage?.isHidden = false
        } else if forecasts[indexPath.row].main == "Rain" {
            cell.weatherImage?.image = UIImage(named: "ic_rain")
            cell.weatherImage?.isHidden = false
        } else if forecasts[indexPath.row].main == "Clear" {
            cell.weatherImage?.image = UIImage(named: "ic_sunny")
            cell.weatherImage?.isHidden = false
        } else if forecasts[indexPath.row].main == "Snow" {
            cell.weatherImage?.image = UIImage(named: "ic_snow")
            cell.weatherImage?.isHidden = false
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
