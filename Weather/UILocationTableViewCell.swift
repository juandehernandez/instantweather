//
//  UILocationTableViewCell.swift
//  Weather
//
//  Created by Juan de la cruz Hernandez Garcia on 30/11/2019.
//  Copyright © 2019 Juan de la cruz Hernandez y Bernardo Nouel. All rights reserved.
//

import UIKit

class UILocationTableViewCell: UITableViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var tempMax: UILabel!
    @IBOutlet weak var tempMin: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
